import csv
import re
import sys
import osmium

tuple_exclusion_par_mot = (" ", "(", ")", "en", "sur", "sous", "saint", "sainte", "de", "du", "d", "des", "l", "le", "la", "les", "st",
                   "et", "ste", "arrondissement", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
                   "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")

tuple_exclusion_par_carac = ("à", "â", "ç", "è", "é", "ê", "î", "ô", "ù", "û", "(", ")", "1", "2", "3", "4", "5", "6", "7", "8", "9")


class OsmHandler(osmium.SimpleHandler):
    def __init__(self):
        super(OsmHandler, self).__init__()
        self.coordpbf = {}
        self.listecpelem = []

    def node(self, elem):
        if elem.tags.get('addr:postcode'):
            self.listecpelem = elem.tags.get('addr:postcode').split(';')
            for each in self.listecpelem:
                if each in self.coordpbf:
                    if elem.tags.get('name'):
                        self.coordpbf[each].append([elem.tags['name'], elem.location.lat, elem.location.lon])
                else:
                    if elem.tags.get('name'):
                        self.coordpbf.update({each: [[elem.tags['name'], elem.location.lat, elem.location.lon]]})


def extraire_donnees_csv(finput):
    reader = csv.reader(finput)
    liste_ville = {}
    for row in reader:
        for each in row:
            list_each = each.split(';')
            norm_cp = normaliser_cp(list_each[1])
            norm_ville = list_each[2]
            user = list_each[0]
            if norm_cp not in liste_ville:
                liste_ville.update({norm_cp: [[norm_ville, user]]})
            else:
                liste_ville[norm_cp].append([norm_ville, user])

    finput.close()
    return liste_ville


def normaliser_ville(nom_ville):
    nom_ville = nom_ville.lower()
    nom_ville = nom_ville.strip()
    nom_ville_carac = [i for i in nom_ville]
    nom_ville_sans_carac = ""
    for each in nom_ville_carac:
        if each not in tuple_exclusion_par_carac:
            nom_ville_sans_carac = nom_ville_sans_carac + each
    nom_ville_sans_carac = re.sub(r'[^\w\s]', ' ', nom_ville_sans_carac)
    nom_ville_sans_carac = re.sub(r'\d+', '', nom_ville_sans_carac)
    nom_ville_decoupe = nom_ville_sans_carac.split(' ')
    liste_nom_ville_epure = []
    for each in nom_ville_decoupe:
        if each not in tuple_exclusion_par_mot:
            each = each.capitalize()
            liste_nom_ville_epure.append(each)
    nom_ville_epure = " ".join(liste_nom_ville_epure)
    return nom_ville_epure


def normaliser_cp(cp):
    if cp.isdigit():
        cp = str(cp)
        if len(cp) < 5:
            while len(cp) < 5:
                cp = f'0{cp}'
        return cp
    else:
        return cp


def extraire_cle_commun():
    h = OsmHandler()
    h.apply_file(sys.argv[2], locations=True)
    ds = [extraire_donnees_csv(open(sys.argv[1], 'r')), h.coordpbf]
    testcsv = {}
    exclucsv = {}
    testpbf = {}
    liste_final_csv = {}
    keys = testcsv.keys()
    for key_csv in ds[0].keys():
        for key_pbf in ds[1].keys():
            if key_csv == key_pbf and key_csv not in testcsv:
                testcsv.update({key_csv: ds[0].get(key_csv)})
                testpbf.update(({key_pbf: ds[1].get(key_pbf)}))
    for key_csv in ds[0].keys():
        if key_csv not in keys:
            get_values = ds[0].get(key_csv)
            exclucsv.update({get_values[0][1]: [key_csv, get_values[0][0]]})

    for key_csv in testcsv:
        temp_value_csv = testcsv.get(key_csv)
        temp_value_pbf = testpbf.get(key_csv)

    for each_csv in temp_value_csv:
        for each_pbf in temp_value_pbf:
            if normaliser_ville(each_csv[0]) == normaliser_ville(each_pbf[0]):
                liste_final_csv[each_csv[1]] =  [key_csv, each_pbf[0], each_pbf[1], each_pbf[2]]
            else:
                # if each_csv[1] not in liste_final_csv.keys():
                exclucsv[each_csv[1]] = [key_csv, each_csv[0]]


# keys = exclucsv.keys()
# temp = []
# for keys_csv in liste_final_csv.keys():
    #     for each in keys:
    #         if each == keys_csv:
    #             temp.append(each)
    # for each in temp:
    #     exclucsv.pop(each)

    # for key_exclucsv in ds[0].keys():
    #     temp_values = ds[0].get(key_exclucsv)
    #     #print(temp_values)
    #     for each in temp_values:
    #         for k in keys:
    #             if k not in each[1]:
    #                 k

    f = open('f_final_csv.txt', 'w')
    for key, values in liste_final_csv.items():
        strrowex = str(key) + str(values) + '\n'
        f.write(strrowex)
    f.close()

    f = open('f_exclu_csv.txt', 'w')
    for key, values in exclucsv.items():
        strrowex = str(key) + str(values) + '\n'
        f.write(strrowex)
    f.close()

    f = open('f_temp_pbf.txt', 'w')
    for key, values in testpbf.items():
        strrowpbf = str(key) + str(values) + '\n'
        f.write(strrowpbf)
    f.close()

    f = open('f_temp_csv.txt', 'w')
    for key, values in testcsv.items():
        strrowcsv = str(key) + str(values) + '\n'
        f.write(strrowcsv)
    f.close()
    # return testcsv, testpbf


extraire_cle_commun()
